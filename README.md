# skill-sample-basic-architecture

Folder architecture model for a basic skill


# Instalação inicial.
No prompt de comando, entrar na pasta lambda e instalar as dependencias

$ npm install


## **** CASO O VSCODE NÃO ESTEJA CONFIGURADO ****
## instalar extensões no VsCode
eslint
prettier
EditorConfig


## configurar prettier no vscode para ler o arquivo de configuração eslint
# Abrir configurações
    ctrl + p
# Entrar em Settings
>settings => preferences: Open Settings (JSON)
# Seta true para format on save
"editor.formatOnSave": true
# Adiciona abaixo a configuração prettier
 "prettier.eslintIntegration": true
